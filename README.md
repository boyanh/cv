# cv1920_assignments
This repository holds the assignments for the lecture "Computer Vision" at Freie Universität Berlin (winter term 2019/2020).

---

### Papers collection

* Canny, 1986 - A Computational Approach to Edge Detection
  * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.420.3300&rep=rep1&type=pdf
* Bradski, 1998 - Computer Vision Face Tracking For Use in a Perceptual User Interface
  * http://opencv.jp/opencv-1.0.0_org/docs/papers/camshift.pdf
* Dalal and Triggs, 2007 - Histograms of Oriented Gradients for Human Detection
  * https://lear.inrialpes.fr/people/triggs/pubs/Dalal-cvpr05.pdf
* Ballard, 1981 - GENERALIZING THE HOUGH TRANSFORM TO DETECT ARBITRARY SHAPES*
  * https://www.cs.bgu.ac.il/~icbv161/wiki.files/Readings/1981-Ballard-Generalizing_the_Hough_Transform_to_Detect_Arbitrary_Shapes.pdf
* Harris and Stephens, 1988 - A combined corner and edge detector
  * http://www.bmva.org/bmvc/1988/avc-88-023.pdf
* Lucas and Kanade, 1981 - An Iterative Image Registration Technique with an Application to Stereo Vision
  * https://ri.cmu.edu/pub_files/pub3/lucas_bruce_d_1981_2/lucas_bruce_d_1981_2.pdf
* Viola and Jones, 2001 - Rapid Object Detection using a Boosted Cascade of Simple
Features
  * https://www.cs.cmu.edu/~efros/courses/LBMV07/Papers/viola-cvpr-01.pdf
